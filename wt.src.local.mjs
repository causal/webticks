export class Source {
    config;

    listeners = new Array();

    constructor(config) {
        console.debug("Source::Local::constructor", config);

        this.config = config;
    }

    async send(type, msg) {
        console.debug("Source::Local::send", type, msg);

        this.trigger({type: type, value: msg});
    }

    on(callback) {
        console.debug("Source::Local::on", callback);

        this.listeners.push(callback);
    }

    once(callback) {
        console.debug("Source::Local::once", callback);
        
        this.onceListeners.push(callback);
    }
    
    off(callback) {
        console.debug("Source::Local::off", callback);

        this.listeners = listeners.filter((value) => !(value === callback));
    }
    
    trigger(...args) {
        console.debug("Source::Local::trigger", ...args);

        if (this.listeners && this.listeners.length) {
            this.listeners.forEach((listener) => {
                listener(...args);
            });
            return true;
        }
        return false;
    }
}