class Space {
    junction; name;

    constructor(junction, name) {
        console.debug("Space::constructor", junction, name);

        this.junction = junction;
        this.name = name;
    }

    async get(key) {
        console.debug("Space::get", this.junction, this.name, key);

        return await this.junction.get(this.name, key);
    }

    async set(key, value) {
        console.debug("Space::set", this.junction, this.name, key, value);

        if(this.junction.get !== undefined)
            await this.junction.set(this.name, key, value);
        else
            console.error(`Space::set`, "junction does not support operation");
    }

    async drop(key) {
        console.debug("Space::drop", this.junction, this.name, key);

        if(this.junction.drop !== undefined)
            await this.junction.drop(this.name, key);
        else
            console.error(`Space::drop`, "junction does not support operation");
    }

    async list() {
        console.debug("Space::list", this.junction, this.name);

        if(this.junction.list !== undefined)
            return await this.junction.list(this.name);
        else
            console.error(`Space::list`, "junction does not support operation");
    }
}

class Channel {
    wt; source; source_name;

    messageListener;

    listeners = new Map();
    onceListeners = new Map();

    connected = false;

    constructor(wt, source_name, source, connect = true) {
        console.debug("Channel::constructor", wt, source_name, source, connect);

        this.wt = wt;
        this.source_name = source_name;
        this.source = source;

        if(connect)
            this.connect();
    }

    _handler(e) {
        if(e.data) {
            if(e.data.cmd == "channel" &&
               e.data.src == this.source_name) {
                console.debug("Channel::_handle", this.source_name, e.data.type, e.data.value);

                this.source.send(e.data.type, e.data.value);
            }
        }
    }

    connect() {
        console.debug("Channel::connect", this.source_name);

        if(!this.connected) {
            this.connected = true;
            this.source.on(msg=>{
                this.trigger(msg.type, msg.value);
            });

            this.messageListener = e=>this._handler(e);
            window.addEventListener("message", this.messageListener);
        } else
            throw {
                scope: "Channel::connect",
                error: "already connected to source"
            };
    }

    disconnect() {
        console.debug("Channel::disconnect", this.source_name);

        window.removeEventListener("message", this.messageListener);
        this.listeners = new Map();
        this.onceListeners = new Map();
    }

    async send(type, msg) {
        console.debug("Channel::send", this.source_name, type, msg);

        await this.source.send(type, msg);
    }

    on(type, callback) {
        console.debug("Channel::on", this.source_name, type, callback);

        this.listeners.has(type) || this.listeners.set(type, []);
        this.listeners.get(type).push(callback);
    }
    
    once(type, callback) {
        console.debug("Channel::once", this.source_name, type, callback);

        this.onceListeners.has(type) || this.onceListeners.set(type, []);
        this.onceListeners.get(type).push(callback);
    }
    
    off(type, callback = true) {
        console.debug("Channel::off", this.source_name, type, callback);

        if (callback === true) {
            this.listeners.delete(type);
            this.onceListeners.delete(type);
        } else {
            let _off = (inListener) => {
                let listeners = inListener.get(type);
                if (listeners) {
                    inListener.set(type, listeners.filter((value) => !(value === callback)));
                }
            };
            _off(this.listeners);
            _off(this.onceListeners);
        }
    }

    proxy(type, msg) {
        console.debug("Channel::proxy", this.source_name, type, msg);
        for(const v in this.wt.views)
            document.getElementById(v).contentWindow.postMessage({
                cmd: "channel",
                src: this.source_name,
                type: type,
                value: msg
            });
    }
    
    trigger(type, ...args) {
        console.debug("Channel::trigger", this.source_name, type, ...args);

        let res = false;
        this.proxy(type, ...args);
        let _trigger = (inListener, type, ...args) => {
            let listeners = inListener.get(type);
            if (listeners && listeners.length) {
                listeners.forEach((listener) => {
                    listener(...args);
                });
                res = true;
            }
        };
        _trigger(this.onceListeners, type, ...args);
        _trigger(this.listeners, type, ...args);
        this.onceListeners.delete(type);
        return res;
    }
}

export class WebTicks {
    root = ""; junctions = {}; spaces = {}; sources = {}; channels = {}; views = {};

    async _load(junction_descs, space_descs, source_descs) {
        console.debug("WebTicks::_load", junction_descs, space_descs, source_descs);

        for(const j of junction_descs) {
            const mod_path = "./wt.junc."+j.type+".mjs";
            console.debug("Loading Junction:", mod_path);
            const {Junction} = await import(mod_path);
            this.junctions[j.name] = new Junction(j.name, j.config);
        }

        for(const s of source_descs) {
            const mod_path = "./wt.src."+s.type+".mjs";
            console.debug("Loading Source:", mod_path);
            const {Source} = await import(mod_path);
            this.sources[s.name] = new Source(s.config);
        }

        for(const s of space_descs) {
            if(this.junctions.hasOwnProperty(s.junction)) {
                this.spaces[s.name] = new Space(this.junctions[s.junction], s.name);
            } else
                throw {
                    scope: "WebTicks::_load",
                    error: `no junction <${s.junction}> found`
                }
        }

        return this;
    }

    async _resolve_get(id, space, key) {
        console.debug("WebTicks::_resolve_get", id, space, key);

        this.get(space, key).then(v=>{
            document.getElementById(id).contentWindow.postMessage({
                cmd: "got",
                space: space,
                key: key,
                value: v
            });
        }).catch(e=>{
            document.getElementById(id).contentWindow.postMessage({
                cmd: "got",
                space: space,
                key: key,
                error: e
            });
        });
    }

    async _resolve_set(id, space, key, value) {
        console.debug("WebTicks::_resolve_set", id, space, key, value);

        this.set(space, key, value).then(()=>{
            document.getElementById(id).contentWindow.postMessage({
                cmd: "sot",
                space: space,
                key: key
            });
        }).catch(e=>{
            document.getElementById(id).contentWindow.postMessage({
                cmd: "sot",
                space: space,
                key: key,
                error: e
            });
        });
    }

    _handler(e) {
        if(e.data) {
            switch(e.data.cmd) {
                case "loaded": {
                    if(this.views.hasOwnProperty(e.data.id)) {
                        console.debug("WebTicks::_handler::loaded", e.data.id, this.views[e.data.id]);

                        document.getElementById(e.data.id).contentWindow.postMessage({
                            cmd: "run",
                            space: this.views[e.data.id].space,
                            tick: this.views[e.data.id].tick,
                            args: this.views[e.data.id].args
                        });
                    }
                    break;
                } case "get": {
                    console.debug("WebTicks::_handler::get", e.data.id, e.data.space, e.data.key, this.views[e.data.id]);

                    this._resolve_get(e.data.id, e.data.space, e.data.key);
                    break;
                } case "set": {
                    console.debug("WebTicks::_handler::set", e.data.id, e.data.space, e.data.key, e.data.value, this.views[e.data.id]);

                    this._resolve_set(e.data.id, e.data.space, e.data.key, e.data.value);
                    break;
                }
            }
        }
    }

    constructor(junction_descs, space_descs, source_descs) {
        console.debug("WebTicks::constructor", junction_descs, space_descs, source_descs);

        this.root = `${window.location.protocol}//${window.location.hostname}:${window.location.port}${window.location.pathname.substring(0,window.location.pathname.lastIndexOf("/"))}`;
       
        const messageListener = e=>this._handler(e);
        window.addEventListener("message", messageListener);
        return this._load(junction_descs, space_descs, source_descs);
    }

    async import(space, path) {
        console.debug("WebTicks::import", space, path, this.junctions);

        if(this.spaces.hasOwnProperty(space))
            return await import("data:text/javascript;base64,"
                + btoa(unescape(encodeURIComponent(await this.get(space, path+".mjs")))));
        else
            throw {
                scope: "WebTicks::import",
                error: `no space <${space}> found`
            };
    }

    async create(id, layout, target, space, tick_name, args, config = {
        class: "",
        style: "",
        frameborder: "0px",
        scrolling: "yes"
    }) {
        console.debug("WebTicks::create", id, layout, target, space, tick_name, args, config);

        if(this.views.hasOwnProperty(id))
            throw {
                scope: "WebTicks::create",
                error: `frame <${id}> already existing`
            };
        else {
            this.views[id] = {
                target: target,
                space: space,
                tick: tick_name,
                args: args,
                config: config
            };

            const {tick} = await this.import(space, tick_name);

            // TODO introduce promise which resolves when frame is loaded
            $("<iframe>", {
                id: id,
                src: `${tick.type}.html?id=${id}&layout=${layout}&timestamp=${Date.now()}`,
                class: config.class,
                style: config.style,
                frameborder: config.frameborder,
                scrolling: config.scrolling
                }).appendTo(target);
            
            return id;
        }
    }

    async destroy(id) {
        console.debug("WebTicks::destroy", id);

        delete this.views[id];
        $("#"+id).remove();
    }

    async get(space, key) {
        console.debug("WebTicks::get", space, key);

        if(this.spaces.hasOwnProperty(space))
            return await this.spaces[space].get(key);
        else
            throw {
                scope: "WebTicks::get",
                error: `no space <${space}> found`
            };
    }

    async set(space, key, value) {
        console.debug("WebTicks::set", space, key, value);

        if(this.spaces.hasOwnProperty(space))
            return await this.spaces[space].set(key, value);
        else
            throw {
                scope: "WebTicks::set",
                error: `no space <${space}> found`
            };
    }

    identify(source, id, phrase) {
        console.debug("WebTicks::identify", source);

        if(this.sources.hasOwnProperty(source) && this.sources[source]) {
            const s = this.sources[source];

            if(typeof s.identify == 'function') {
                s.identify(id, phrase);
            }
        }
    }

    leave(source) {
        console.debug("WebTicks::leave", source);

        if(this.sources.hasOwnProperty(source) && this.sources[source]) {
            const s = this.sources[source];

            if(s.hasOwnProperty('leave') && typeof s.leave == 'function')
                s.leave();
        }
    }

    channel(source, connect = true) {
        console.debug("WebTicks::channel", source, connect);

        if(this.channels.hasOwnProperty(`${source}`))
            return this.channels[`${source}`];
        else {
            const chan = new Channel(this, source, this.sources[source], connect);
            this.channels[`${source}`] = chan;

            return chan;
        }
    }
}

class ChannelProxy {
    id; source;

    listeners = new Map();
    onceListeners = new Map();

    connected = false;

    constructor(id, source, connect = true) {
        console.debug("ChannelProxy::constructor", id, source, connect);

        this.id = id;
        this.source = source;

        if(connect)
            this.connect();
    }

    _handler(e) {
        if(e.data) {
            if(e.data.cmd == "channel" &&
               e.data.src == this.source) {
                console.debug("ChannelProxy::_handle", this.id, this.source, e.data.type, e.data.value);

                this.trigger(e.data.type, e.data.value);
            }
        }
    }

    connect() {
        console.debug("ChannelProxy::connect", this.id, this.source);

        if(!this.connected) {
            this.connected = true;

            // TODO disconnect
            const messageListener = e=>this._handler(e);
            window.addEventListener("message", messageListener);
        } else
            throw {
                scope: "Channel::connect",
                error: "already connected to source"
            };
    }

    disconnect() {
        console.debug("ChannelProxy::disconnect", this.id, this.source);

        this.listeners = new Map();
        this.onceListeners = new Map();
    }

    async send(type, msg) {
        console.debug("ChannelProxy::send", this.id, this.source, type, msg);
        
        window.parent.postMessage({
            cmd: "channel",
            src: this.source,
            type: type,
            value: msg
        });
        //this.trigger(type, msg);
    }

    on(type, callback) {
        console.debug("ChannelProxy::on", this.id, this.source, type, callback);

        this.listeners.has(type) || this.listeners.set(type, []);
        this.listeners.get(type).push(callback);
    }
    
    once(type, callback) {
        console.debug("ChannelProxy::once", this.id, this.source, type, callback);

        this.onceListeners.has(type) || this.onceListeners.set(type, []);
        this.onceListeners.get(type).push(callback);
    }
    
    off(type, callback = true) {
        console.debug("ChannelProxy::off", this.id, this.source, type, callback);

        if (callback === true) {
            this.listeners.delete(type);
            this.onceListeners.delete(type);
        } else {
            let _off = (inListener) => {
                let listeners = inListener.get(type);
                if (listeners) {
                    inListener.set(type, listeners.filter((value) => !(value === callback)));
                }
            };
            _off(this.listeners);
            _off(this.onceListeners);
        }
    }
    
    trigger(type, ...args) {
        console.debug("ChannelProxy::trigger", this.id, this.source, type, ...args);

        let res = false;
        let _trigger = (inListener, type, ...args) => {
            let listeners = inListener.get(type);
            if (listeners && listeners.length) {
                listeners.forEach((listener) => {
                    listener(...args);
                });
                res = true;
            }
        };
        _trigger(this.onceListeners, type, ...args);
        _trigger(this.listeners, type, ...args);
        this.onceListeners.delete(type);
        return res;
    }
}

export class WebTicksProxy {
    id; build; channels = {}; views = {};
    _get_promises = {}; _set_promises = {};
    tick = null;

    constructor(id) {
        console.debug("WebTicksProxy::constructor", id);

        this.id = id;
    }

    async _handler(e) {
        if(e.data) {
            switch(e.data.cmd) {
                case "run": {
                    console.debug("WebTicksProxy::_handler::run", e.data.space, e.data.tick);

                    const {tick} = await this.import(e.data.space, e.data.tick);
                    try {
                        this.tick = new tick(this, e.data.args);
                    } catch(err) {
                        tick.fail(this, err);
                    }
                    break;
                } case "got": {
                    console.debug("WebTicksProxy::_handler::got", e.data.space, e.data.key);

                    const p = this._get_promises[`get>${e.data.space}::${e.data.key}`].shift();
                    if(typeof e.data.error == "undefined")
                        p.resolve(e.data.value);
                    else
                        p.reject(e.data.error);
                    break;
                } case "sot": {
                    console.debug("WebTicksProxy::_handler::sot", e.data.space, e.data.key);

                    const p = this._set_promises[`set>${e.data.space}::${e.data.key}`].shift();
                    if(typeof e.data.error == "undefined")
                        p.resolve(e.data.value);
                    else
                        p.reject(e.data.error);
                    break;
                }
            }
        }
    }

    connect() {
        console.debug("WebTicksProxy::connect", this.id);

        const messageListener = e=>this._handler(e);
        window.addEventListener("message", messageListener);
        window.parent.postMessage({cmd: "loaded", id: this.id});
    }

    async import(space, path) {
        console.debug("WebTicksProxy::import", this.id, space, path);

        const data = await this.get(space, path+".mjs");
        return await import("data:text/javascript;base64,"
                + btoa(unescape(encodeURIComponent(data))));
    }

    async create(id, layout, target, space, tick_name, args, config = {
        class: "",
        style: "",
        frameborder: "0px",
        scrolling: "yes"
    }) {
        console.debug("WebTicksProxy::create", this.id, id, layout, target, space, tick_name, args, config);

        if(this.views.hasOwnProperty(id))
            throw {
                scope: "WebTicksProxy::create",
                error: `frame <${id}> already existing`
            };
        else {
            this.views[id] = {
                target: target,
                space: space,
                tick: tick_name,
                args: args,
                config: config
            };

            const tick = await this.import(space, tick_name);

            // TODO introduce promise which resolves when frame is loaded
            $("<iframe>", {
                id: id,
                src: `${tick.type}.html?id=${id}&layout=${layout}&timestamp=${Date.now()}`,
                name: id,
                class: config.class,
                style: config.style,
                frameborder: config.frameborder,
                scrolling: config.scrolling
                }).appendTo(target);
            
            return id;
        }
    }

    get(space, key) {
        console.debug("WebTicksProxy::get", this.id, space, key);

        const type = `get>${space}::${key}`;

        const p = {};
        p.promise = new Promise((res, rej)=>{
            p.resolve = res;
            p.reject = rej;
        });

        if(this._get_promises.hasOwnProperty(type))
            this._get_promises[type].push(p);
        else
            this._get_promises[type] = [p];

        window.parent.postMessage({
            cmd: "get",
            id: this.id,
            space: space,
            key: key
        });
        return p.promise;
    }

    set(space, key, value) {
        console.debug("WebTicksProxy::set", this.id, space, key, value);

        const type = `set>${space}::${key}`;

        const p = {};
        p.promise = new Promise((res, rej)=>{
            p.resolve = res;
            p.reject = rej;
        });

        if(this._set_promises.hasOwnProperty(type))
            this._set_promises[type].push(p);
        else
            this._set_promises[type] = [p];

        window.parent.postMessage({
            cmd: "set",
            id: this.id,
            space: space,
            key: key,
            value: value
        });
        return p.promise;
    }

    channel(source, connect = true) {
        console.debug("WebTicksProxy::channel", this.id, source, connect);

        if(this.channels.hasOwnProperty(`${source}`))
            return this.channels[`${source}`];
        else {
            const chan = new ChannelProxy(this.id, source, connect);
            this.channels[`${source}`] = chan;

            return chan;
        }
    }
}