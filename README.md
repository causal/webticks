# WebTicks
WebTicks is a javascript framework for hosting user interaction ticks which could be part of some causal dynamics or just execute DOM oriented javascript snippets in a structured way.

## License
GNU Affero General Public License v3