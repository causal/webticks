export class tick {
    static name = ">test tick";
    static type = "proxy";

    wt = null;
    tst_chan = null;

    constructor(wt, args) {
        console.debug("test::tick::constructor", wt, args);

        this.wt = wt;
        this.tst_chan = this.wt.channel("test");

        this.tst_chan.on("channel-trigger-msg", msg=>{
            console.debug("test::tick::tst_chan.on.channel-trigger-msg", wt.id, msg);
            
            this.tst_chan.send("test-msg", msg);
        });

        this.tst_chan.on("space-cookie-trigger-msg", async msg=> {
            console.debug("test::tick::tst_chan.on.channel-trigger-msg", wt.id, msg);
            
            const old = await this.wt.get("cookie", msg);
            this.wt.set("cookie", msg, old + 1);
            this.tst_chan.send("test-msg", msg);
        });

        this.tst_chan.on("space-site-trigger-msg", async msg=> {
            console.debug("test::tick::tst_chan.on.channel-trigger-msg", wt.id, msg);
            
            const v = await this.wt.get("site", msg);
            this.tst_chan.send("test-msg", v);
        });

        this.tst_chan.send("result", "test_tick_loaded");
    }

    static fail(wt, err) {
        console.debug("test::tick::fail", wt, err);

        let tst_chan = wt.channel("test");
        tst_chan.send("result", "test_tick_failed");
    }
};
