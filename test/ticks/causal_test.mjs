export class tick {
    static name = ">causal test tick";
    static type = "proxy";

    wt = null;
    tst_chan = null; tst_chan_causal = null;

    constructor(wt, args) {
        console.debug("causal_test::tick::constructor", wt, args);

        this.wt = wt;
        this.tst_chan = this.wt.channel("test");
        this.tst_chan_causal = this.wt.channel("causal");

        this.tst_chan_causal.on("causal::data::IdentifyResponse", msg=>{
            console.debug("causal_test::tick::tst_chan_causal.causal::data::IdentifyResponse", msg);

            this.tst_chan.send("test-msg", "IdentifyResponse");
        });

        this.tst_chan.on("channel-trigger-send-msg", async msg=> {
            console.debug("causal_test::tick::tst_chan.on.channel-trigger-send-msg", wt.id, msg);
            
            this.tst_chan_causal.send("TestRequest", {
                "msg": "TEST1"
            });
        });

        this.tst_chan_causal.on("TestResponse", msg=>{
            console.debug("causal_test::tick::tst_chan_causal.TestResponse", msg);

            if(msg.msg == "TEST1")
                this.tst_chan.send("test-msg", "TestResponse");
            else
                console.error("causal_test::tick::tst_chan_causal.TestResponse", "wrong msg", msg.msg);
        });

        this.tst_chan.send("result", "test_tick_loaded");
    }

    static fail(wt, err) {
        console.debug("causal_test::tick::fail", wt, err);

        let tst_chan = wt.channel("test");
        tst_chan.send("result", "test_tick_failed");
    }
};
