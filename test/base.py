import unittest, functools, traceback
import os, shutil, sys, json, base64
import http.server, threading

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class WebTestCase(unittest.TestCase):
    type = os.environ["WT_TEST_BROWSER"] if "WT_TEST_BROWSER" in os.environ else "chromium"

    def __init__(self, name: str, site: str, params: dict):
        super(WebTestCase, self).__init__(name)
        self.site = site
        self.params = params

    def setUp_chromium(self):
        options = webdriver.chrome.options.Options()
        options.add_argument("--headless=new")
        options.add_argument(f"--user-data-dir={os.getcwd()}/test/__profile__")
        options.set_capability("goog:loggingPrefs", {'browser': 'ALL'})
        self.browser = webdriver.Chrome(options=options)

    def setUp_firefox(self):
        options = webdriver.firefox.options.Options()
        options.add_argument("-headless")
        self.browser = webdriver.Firefox(options=options)

    def setUp(self):
        try:
            if os.path.exists(f"{os.getcwd()}/test/__profile__"):
                shutil.rmtree(f"{os.getcwd()}/test/__profile__")

            match self.type:
                case "firefox":
                    self.setUp_firefox()
                case _:
                    self.setUp_chromium()

            #params = base64.urlsafe_b64encode(json.dumps(self.params).encode('utf-8'))
            params = base64.urlsafe_b64encode(json.dumps(self.params).encode('ascii')).decode("ascii")
            url = f"http://127.0.0.1:58000/test/{self.site}.html?params={params}"
            self.browser.get(url)
            WebDriverWait(self.browser, timeout=2).until(
                EC.element_attribute_to_include((By.TAG_NAME, "body"), "loaded")
            )
        except:
            self.tearDown()
            raise

    def tearDown(self):
        if hasattr(self, 'browser'):
            try:
                if not self.type == "firefox":
                    for entry in self.browser.get_log('browser'):
                        print(json.dumps(entry, indent=4))
            except:
                print(traceback.format_exc())
            self.browser.quit()
        if os.path.exists(f"{os.getcwd()}/test/__profile__"):
            shutil.rmtree(f"{os.getcwd()}/test/__profile__")

class ServedWebTestCase(WebTestCase):
    class _SimpleRequestHandler(http.server.SimpleHTTPRequestHandler):
        """Same as SimpleHTTPRequestHandler with adjusted logging."""

        def log_message(self, format, *args):
            http.server.SimpleHTTPRequestHandler.log_message(self, format, *args)

    def __init__(self, name: str, site: str, params: dict):
        super(ServedWebTestCase, self).__init__(name, site, params)

    def setUp_httpd(self):
        self.handler = functools.partial(ServedWebTestCase._SimpleRequestHandler, directory="./")
        self.httpd = http.server.HTTPServer(("127.0.0.1", 58000), self.handler, False)
        self.httpd.timeout = 1
        self.httpd.server_bind()
        self.httpd.server_activate()

        self.thread = threading.Thread(target=ServedWebTestCase.serve_forever, args=(self, ))
        self.thread.daemon = True
        self.thread.start()

    def serve_forever(self):
        with self.httpd:
            self.httpd.serve_forever()

    def setUp(self):
        try:
            self.setUp_httpd()
        except:
            self.tearDown()
            raise

        super(ServedWebTestCase, self).setUp()

    def tearDown(self):
        super(ServedWebTestCase, self).tearDown()

        if hasattr(self, 'httpd') and hasattr(self, 'thread'):
            self.httpd.shutdown()
            self.thread.join()