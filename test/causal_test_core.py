from base import *

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class CausalTestCore(WebTestCase):
    init = """
        console.debug("test_login");
        document.tst_chan_causal = document.wt.channel("causal");
        document.wt.identify("causal", "testuser", "testpassword");
    """

    def __init__(self, name: str):
        super(CausalTestCore, self).__init__(name, "base", {
            "sources": [{"type": "causal", "name": "causal", "config": {"url": "http://127.0.0.1:58000/_"}}],
            "junctions": [],
            "spaces": [],
            "tick": "causal_test",
            "tick_args": ""
        })

    def test_title(self):
        self.assertEqual(self.browser.title, 'WebTicks Test Base')

    def login(self):
        self.browser.execute_script(self.init)
        
        WebDriverWait(self.browser, timeout=5).until(
            EC.element_attribute_to_include((By.TAG_NAME, "body"), "received")
        )
        self.assertEqual(
            self.browser.find_element(By.TAG_NAME, "body").get_attribute("received"),
            "IdentifyResponse"
        )

    def test_login(self):
        self.login()

        self.browser.execute_script("""
            document.wt.leave("causal");
        """)
        pass

    def test_ping_pong(self):
        self.login()

        self.browser.execute_script("""
            document.tst_chan.send("channel-trigger-send-msg", "test_receive");
        """)
        
        WebDriverWait(self.browser, timeout=5).until(
            lambda d: d.execute_script("console.warn($('body').attr('received')); return $('body').attr('received')") == 'IdentifyResponse,TestResponse'
        )

        self.browser.execute_script("""
            document.wt.leave("causal");
        """)

if __name__ == '__main__':
    unittest.main()