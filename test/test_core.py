from base import *

from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class TestCore(ServedWebTestCase):
    def __init__(self, name: str):
        super(TestCore, self).__init__(name, "base", {
            "sources": [],
            "junctions": [],
            "spaces": [],
            "tick": "test",
            "tick_args": ""
        })

    def test_title(self):
        self.assertEqual(self.browser.title, 'WebTicks Test Base')

    def test_channel_loop(self):
        self.browser.execute_script("document.tst_chan.send('channel-trigger-msg', 'test_channel_loop')")
        
        WebDriverWait(self.browser, timeout=5).until(
            EC.element_attribute_to_include((By.TAG_NAME, "body"), "received")
        )
        self.assertEqual(
            self.browser.find_element(By.TAG_NAME, "body").get_attribute("received"),
            "test_channel_loop"
        )

    def test_space_cookie(self):
        self.browser.execute_script("""
            document.wt.set('cookie', 'test_space_cookie', 1);
            document.tst_chan.send('space-cookie-trigger-msg', 'test_space_cookie');
        """)
        
        WebDriverWait(self.browser, timeout=5).until(
            EC.element_attribute_to_include((By.TAG_NAME, "body"), "received")
        )
        self.assertEqual(
            self.browser.find_element(By.TAG_NAME, "body").get_attribute("received"),
            "test_space_cookie"
        )

        self.browser.execute_script("document.wt.get('cookie', 'test_space_cookie').then(v=>{$('body').attr('valued', v)});")

        WebDriverWait(self.browser, timeout=5).until(
            EC.element_attribute_to_include((By.TAG_NAME, "body"), "valued")
        )

        self.assertEqual(
            self.browser.find_element(By.TAG_NAME, "body").get_attribute("valued"),
            "2"
        )

    def test_space_site(self):
        self.browser.execute_script("""
            document.wt.set('cookie', 'test_space_site', 1);
            document.tst_chan.send('space-site-trigger-msg', 'test_space_site.txt');
        """)
        
        WebDriverWait(self.browser, timeout=5).until(
            EC.element_attribute_to_include((By.TAG_NAME, "body"), "received")
        )
        self.assertEqual(
            self.browser.find_element(By.TAG_NAME, "body").get_attribute("received"),
            "test_space_site"
        )

        self.browser.execute_script("document.wt.get('site', 'test_space_site.txt').then(v=>{$('body').attr('valued', v)});")

        WebDriverWait(self.browser, timeout=5).until(
            EC.element_attribute_to_include((By.TAG_NAME, "body"), "valued")
        )

        self.assertEqual(
            self.browser.find_element(By.TAG_NAME, "body").get_attribute("valued"),
            "test_space_site"
        )

if __name__ == '__main__':
    unittest.main()