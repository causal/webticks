export class Junction {
    name; config;

    constructor(name, config) {
        console.debug("Junction::Site::constructor", name, config);

        this.name = name;
        this.config = config;
    }

    async get(space, key) {
        console.debug("Junction::Site::get", space, key);

        const ts = Date.now();
        const r = await fetch(`${this.config.path}/${space}/${key}?timestamp=${ts}`, {
            headers: {
              "Content-Type": "javascript/esm",
            },
            credentials: 'include'
        });
        return r.text();
    }

    set = undefined;
    drop = undefined;
    list = undefined;
}