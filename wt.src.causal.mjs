export class Source {
    static poll_intervall = 0.2;

    config = null;

    out_type_cache = new Map();
    in_type_cache = new Map();

    interval = null;

    listeners = new Array();

    sign = null;

    constructor(config) {
        if(!config.hasOwnProperty('url') || !config.url)
            throw {
                scope: "Source::Causal::constructor",
                error: "no url given (eg, /source, https://www.foo.bar:12345/source)"
            }

        console.debug("Source::Causal::constructor", config.url);

        this.config = config;

        fetch(`${this.config.url}`, {
            headers: {
                "Content-Type": "application/json",
            },
            credentials: 'include'
        })
            .then(async r=> {
                const data = await r.json();
                this.session = data["session"];
            })
            .catch(e=> console.error(e));
    }

    decode_type(type) {
        console.debug("Source::Causal::decode_type", this.config.url, type);

        if(this.in_type_cache.has(type))
            return this.in_type_cache[type];

        let parts = [];
        if(type.startsWith("N")) {
            // Remove the leading 'N' character and trailing 'E'
            type = type.slice(1, -1);
            
            let current_part = '';
            for (let i = 0; i < type.length; ++i) {
                let char = type[i];
                if (char >= '0' && char <= '9') {
                    // Extract the length of the next part
                    let lengthStr = '';
                    while (char >= '0' && char <= '9') {
                        lengthStr += char;
                        char = type[++i];
                    }
                    const length = parseInt(lengthStr, 10);
                    current_part = type.slice(i, i + length);
                    parts.push(current_part);
                    i += length - 1; // Move the index to the end of the current part
                } else {
                    current_part += char;
                }
            }
        } else {
            parts.push(type.replace(/^\d+/, ''));
        }

        const decoded = parts.join('::');
        this.in_type_cache.set(type, decoded);
        return decoded;
    }

    encode_type(type) {
        console.debug("Source::Causal::encode_type", this.config.url, type);

        if(this.out_type_cache.has(type))
            return this.out_type_cache[type];

        if(type.startsWith("::"))
            type = "causal::data"+type;

        const parts = type.split('::');
        
        let encoded = parts.length > 1 ? 'N' : '';
        for (const part of parts) {
            encoded += part.length.toString() + part;
        }

        if(parts.length > 1) {
            encoded += 'E';
            this.out_type_cache.set(type, encoded);
        }
        return encoded;
    }

    pack(type, value) {
        console.debug("Source::Causal::pack", this.config.url, type, value);

        return {
            "queue":[
                {   "type": this.encode_type(type),
                    "data": value
                }
            ],
            "session":this.session
        }
    }

    unpack(data) {
        console.debug("Source::Causal::unpack", this.config.url, data);

        return {type: this.decode_type(data["type"]), value: data["data"]};
    }

    forward(data) {
        console.debug("Source::Causal::forward", this.config.url, data);

        data.forEach(d=>{
            const msg = this.unpack(d);
            // TODO if causal::data::IdentifyResponse do stuff before forwarding it
            if(msg.type == "causal::data::IdentifyResponse") {
                console.debug("Source::Causal::forward::foreach", msg.value.error, msg.value.sign);
                const r = msg.value;

                if(r.error)
                    console.error("Source::Causal", "error at login", r.error);
                else if(r.sign) {
                    this.sign = r.sign;
                }
            }
            this.trigger(msg);
        });
    }

    poll() {
        console.debug("Source::Causal::poll", this.config.url);
        /* stop with `clearInterval(interval);`*/

        fetch(`${this.config.url+this.session}`, {
            headers: {
              "Content-Type": "application/json",
            },
            credentials: 'include'
        })  .then(async r=> {
                const data = await r.json();
                if(this.session == data["session"])
                    this.forward(data["queue"]);
                else
                    console.warn("invalid session", this.session, data["session"]);
            }).catch(e=> console.error(e));
    }

    identify(id, phrase) {
        console.debug("Source::Causal::identify", this.config.url, id, phrase);

        const data = id == "anonymous"
            ? this.pack("causal::data::AnonRequest", {
                "session": this.session
            })
            : this.pack("causal::data::IdentifyRequest", {
                "session": this.session,
                "id": id,
                "phrase": phrase
            });

        fetch(`${this.config.url+this.session}`, {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(data),
            credentials: 'include'
        })  .then(async r=> {
                this.interval = setInterval(()=>{
                    this.poll();
                }, 0.2);
            }).catch(e=> console.error(e));
    }

    async send(type, msg) {
        console.debug("Source::Causal::send", this.config.url, type, msg);
        
        fetch(`${this.config.url+this.session}`, {
            method: 'POST',
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(this.pack(type, msg)),
            credentials: 'include'
        })  .catch(e=> console.error(e));

        this.trigger({type: type, value: msg});
    }

    on(callback) {
        console.debug("Source::Causal::on", this.config.url, callback);

        this.listeners.push(callback);
    }

    once(callback) {
        console.debug("Source::Causal::once", this.config.url, callback);
        
        this.onceListeners.push(callback);
    }
    
    off(callback) {
        console.debug("Source::Causal::off", this.config.url, callback);

        this.listeners = listeners.filter((value) => !(value === callback));
    }
    
    trigger(...args) {
        console.debug("Source::Causal::trigger", this.config.url, ...args);

        if (this.listeners && this.listeners.length) {
            this.listeners.forEach((listener) => {
                listener(...args);
            });
            return true;
        }
        return false;
    }

    leave() {
        console.debug("Source::Causal::leave", this.config.url);

        // TODO send leave request
        // TODO stop retreive and unpack
    }
}