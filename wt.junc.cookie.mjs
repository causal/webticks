export class Junction {
    name; config;

    constructor(name, config) {
        console.debug("Junction::Cookie::constructor", name, config);

        this.name = name;
        this.config = config;
    }

    async get(space, key) {
        console.debug("Junction::Cookie::get", this.name, space, key);

        const cookie = await Cookies.get(`junc_${this.name}|${space}::${key}`);
        return cookie === undefined ? undefined : JSON.parse(cookie);
    }

    async set(space, key, value) {
        console.debug("Junction::Cookie::set", this.name, space, key, value);

        await Cookies.set(`junc_${this.name}|${space}::${key}`, JSON.stringify(value));
    }

    async drop(space, key) {
        console.debug("Junction::Cookie::drop", this.name, space, key);

        await Cookies.remove(`junc_${this.name}|${space}::${key}`);
    }

    async list(space) {
        console.debug("Junction::Cookie::list", this.name, space);

        const cookies = await Cookies.get();
        let keys = [];
        for(const k in cookies) {
            if(k.startsWith(`junc_${this.name}|${space}::`))
                keys.push(k);
        }
        return keys;
    }
}