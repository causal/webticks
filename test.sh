#!/bin/bash

set -e

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

export PATH=$PATH:$DIR/tmp/bin
   
if [ ! -f "$DIR/tmp/bin/geckodriver" ]; then
    mkdir -p $DIR/tmp/bin
    cd $DIR/tmp/bin
    wget https://github.com/mozilla/geckodriver/releases/download/v0.35.0/geckodriver-v0.35.0-linux64.tar.gz
    tar xzf geckodriver-v0.35.0-linux64.tar.gz
fi

cd $DIR
echo "--------------------------------------------------------------"
echo "CHROMIUM"
echo "--------------------------------------------------------------"
python3 -m unittest discover -s ./test -p "${1:-"test"}_*.py"
echo "--------------------------------------------------------------"
echo ""
echo "--------------------------------------------------------------"
echo "FIREFOX"
echo "--------------------------------------------------------------"
WT_TEST_BROWSER=firefox python3 -m unittest discover -s ./test -p "${1:-"test"}_*.py"
echo "--------------------------------------------------------------"